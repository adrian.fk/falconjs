import FalconJS from "./FalconJS/FalconJS.js";
import App from "./src/App.js";

const app = new App();

FalconJS.render(app);
