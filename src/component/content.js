import FalconJS from "../../FalconJS/FalconJS.js";
import Stage from "../../FalconJS/FalconJS/elements/Stage.js";

FalconJS.importCSS("./src/styles/content.css")

export default
class Content extends FalconJS.Component {


    constructor() {
        super("content");
        this.state = {
            text: "Hello World",
            clicked: false,
        }
    }

    handleClick() {
        if (!this.state.clicked) this.setState({text: "clicked", clicked: true})
        else this.setState({text: "Ehlo world", clicked: false});
    }

    onRender() {
        const clickBtn = Stage.getElement("clickBtn");
        clickBtn.onclick = () => this.handleClick();

    }

    render() {
        return `
            <div id="${this.id}" class="content">
                <h1>${this.state.text}</h1>
                <button id="clickBtn">Click me</button>
            </div>
        `
    }
}