import FalconJS from "../../../FalconJS/FalconJS.js";
import Logo from "./Logo.js";
import Menu from "./Menu.js";



FalconJS.importCSS("./src/styles/header/header.css");


export default
class Header extends FalconJS.Component{

    constructor() {
        super("header");
    }

    render() {
        const headerLogo = new Logo("logo")
        const headerMenu = new Menu("menu");

        return `
            <div id="${this.id}" class="${this.id}">
                ${this.mount(headerLogo)}
                ${this.mount(headerMenu)}
            </div>
        `;
    }
}