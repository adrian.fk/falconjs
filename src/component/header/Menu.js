import FalconJS from "../../../FalconJS/FalconJS.js";




export default
class Menu extends FalconJS.Component {

    constructor(id) {
        super(id);
    }

    render() {
        return `
            <div class="header__menu">
                <div id="menu-item_home" class="header__menu-item">
                    <h4>Home</h4>
                </div>
                <div id="menu-item_about" class="header__menu-item">
                    <h4>About</h4>
                </div>
                <div id="menu-item_bookings" class="header__menu-item">
                    <h4>Bookings</h4>
                </div>
                <div id="menu-item_contact" class="header__menu-item">
                    <h4>Contact</h4>
                </div>
            </div>
        `
    }
}