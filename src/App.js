import FalconJS from "../FalconJS/FalconJS.js";
import Header from "./component/header/Header.js";
import Content from "./component/content.js";


FalconJS.importCSS("./src/App.css");

export default
class App extends FalconJS.Component {

    constructor() {
        super("app");
    }

    render() {
        this.header = new Header();
        const content =  new Content();

        return `
            <div id="app" class="app">
                ${this.mount(this.header)}
                ${this.mount(content)}
            </div>
        `
    }
}