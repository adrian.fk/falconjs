//Facade


import StylesheetManager from "./FalconJS/StylesheetManager.js";
import Component from "./FalconJS/elements/Component.js";
import Page from "./FalconJS/elements/Page.js";
import NavController from "./FalconJS/NavController.js";
import Stage from "./FalconJS/elements/Stage";
import Scene, {scene} from "./FalconJS/elements/Scene";




export const navController = NavController;
export const scene = scene;


export default
class FalconJS {
    static NavController = NavController;

    static Stage = Stage;
    static Scene = Scene;

    static Component = Component;
    static Page = Page;

    ///////////////////////////////////////    SUPPORTIVE PROCEDURES   ////////////////////////////////////////////////
    static importCSS(cssPath) {
        StylesheetManager.linkCss(cssPath);
    }





    /////////////////////////////////////////    RENDER PROCEDURES   //////////////////////////////////////////////////

    static render(view) {
        view.renderToScreen();
    }

}

export function importCSS(path) {
    FalconJS.importCSS(path);
}