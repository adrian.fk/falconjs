
const StylesheetManager = (() => {
    const cssReferences = [];

    const linkCss = (ref) => {
        const header = document.head;
        if (ref && !cssReferences.includes(ref, 0)) {
            cssReferences.push(ref);
            header.insertAdjacentHTML('beforeend', '\n' + '<link rel="stylesheet" href="' + ref + '" type="text/css">');
        }
    }

    const reLinkCss = () => {
        const prevCssRef = [...cssReferences];
        cssReferences.length = 0;
        prevCssRef.forEach(
            cssRef => linkCss(cssRef)
        )
    };


    return { linkCss, reLinkCss }
})();

export default StylesheetManager;