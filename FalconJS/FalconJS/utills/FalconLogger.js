function getPrefix() {
    const now = new Date();
    return now.getDate() + "/" + now.getMonth() + "/" + now.getFullYear() + " @ " + now.getHours() + ":"
        + now.getMinutes() + ":" + now.getSeconds() + " :: "
}

export default class Log {
    static s(string) {
        console.log(getPrefix() + string + "");
    }

    static d(TAG, string) {
        console.log(getPrefix() + TAG + " ::> " + string);
    }
}
