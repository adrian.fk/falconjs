import Log from "./FalconLogger.js";

export function requiredParameter(TAG = "", parameterNameString = "", title) {
    const eString = `${title} --> Parameter ${parameterNameString} missing`;
    Log.d(TAG, eString);
    throw new Error(eString);
}
