

//Super class for all types of ViewGroups: I.e. Pages or Components

import {requiredParameter} from "../utills/Tools.js";
import RenderEngine from "../render/RenderEngine.js";
import StylesheetManager from "../StylesheetManager.js";
import Stage from "./Stage.js";



const TAG = "View";

export default
class View {

    constructor(id = requiredParameter(TAG, "id", "constructor") || "",
                innerHtml = "") {
        this.id = id;

        this.domElement = null;

        this.innerHtml = innerHtml;

        this.parentComponent = null;

        this.mountedComponents = [];
    }

    render() {
        //Overridable method to enable client to specify html to be rendered for this View
        return this.innerHtml;
    }

    setParentComponent(parentComponent) {
        this.parentComponent = parentComponent;
        return this;
    }

    renderToScreen() {
        this.innerHtml = this.render();

        if (!this.getDomElement()) {
            if (this.parentComponent)  {
                const parentStage = Stage.getElement(this.parentComponent.id);
                RenderEngine.renderToDomElement(parentStage, this.innerHtml);
            }
            else {
                //If doesnt exist then add div and fill with view render
                RenderEngine.renderWrapperToRoot(this.id, this.innerHtml);
            }
        }
        else {
            RenderEngine.renderToDomElement(this.getDomElement(), this.innerHtml);
        }
    }

    /**
     * @returns {null} if domElement doesn't exist, and returns {domElement} if it exists in the document
     */
    getDomElement() {
        this.domElement = document.getElementById(this.id);
        return this.domElement;
    }












    getInnerHtml() {
        return this.innerHtml;
    }

    setInnerHtml(html = "") {
        this.innerHtml = html;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        if (id) this.id = id;
    }

    //A little nugget for all views, cus they will need it. The implementor can use this in its constructor or
    // onRender method.
    importCss(cssFilePath) {
        StylesheetManager.linkCss(cssFilePath);
    }
}