import View from "./View.js";

export default
class Component extends View {

    constructor(id, innerHtml) {
        super(id, innerHtml);

        this.state = {};
    }

    setState(state) {
        this.state = {
            ...this.state,
            ...state
        };

        //Re-render
        this.renderToScreen();
    }

    renderToScreen() {
        this.componentWillMount();
        this.beforeRender();
        super.renderToScreen();
        this.onRender();
        this.componentHasMounted();
    }


    /**
     * @objective: to mount a sub component to a parent component such that the onRender and beforeRender methods
     *              gets called upon actual render of component.
     * @param viewComponent: view component to be mounted to this component.
     * @return {string} String with inner html of passed component such that it can be used in return statement
     *                  template literal by client code.
     *
     */
    mount(viewComponent) {
        this.mountedComponents.push(viewComponent);
        return viewComponent.render();
    }

    componentWillMount() {
        this.mountedComponents.map(
            (component, i) => {
                component.componentWillMount();
                component.beforeRender();
            }
        )
    }

    componentHasMounted() {
        this.mountedComponents.map(
            (component, i) => {
                component.onRender();
                component.componentHasMounted();
            }
        )
    }


    beforeRender(handler) {
        //Method the client con override to append functionality before render of component
        if (handler) handler();
    }

    onRender(handler) {
        //Method the client can override to append functionality after the component has rendered
        if (handler) handler(this.innerHtml);
    }
}