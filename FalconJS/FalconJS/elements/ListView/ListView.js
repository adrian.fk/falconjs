import View from "../View.js";
import Log from "../../utills/FalconLogger.js";
import ListViewHolder from "./ListViewHolder.js";
import ListViewAdapter from "./ListViewAdapter.js";



const TAG = "ListView";


export default
class ListView extends View {

    static ListViewHolder = ListViewHolder;
    static ListViewAdapter = ListViewAdapter;


    constructor(id, classNames) {
        super(id, classNames);
        this.adapter = null;
    }

    setListViewAdapter(listViewAdapter) {
        this.adapter = listViewAdapter;
        return this;
    }

    setScrollBar(boolShowScrollBar) {
        this.showScrollBar = boolShowScrollBar;
    }

    render() {
        if (this.adapter) {
            const listViewTag = `<div id="${this.id}" class="${this.classNames ? this.classNames : ""}">`;
            const listViewTagEnd = `</div>`;

            let html = listViewTag;

            for (let index = 0, pos = index + 1; index < this.adapter.getNumItems(); index++, pos++) {
                const viewHolder = this.adapter.onCreateViewHolder();
                html += viewHolder.render();
            }
            html += listViewTagEnd;

            return html;
        }
        else {
            Log.d(TAG, "ListView " + this.id + " not configured with a ListViewAdapter before render() is invoked..");
            return "";
        }
    }
}