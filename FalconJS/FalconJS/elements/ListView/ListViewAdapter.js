

export default
class ListViewAdapter {

    /**
     * @return: a created view holder.
     */
    onCreateViewHolder() {};

    /**
     * @return: number of views that is going to be listed in the list.
     */
    getNumItems() {};
}