import Stage from "./Stage";


export const scene = (sceneId) => {
    let id = sceneId;

    const getScene = (id) => {
        return Stage.getElementById(id);
    }

    const setId = (sceneId) => {
        id = sceneId;
    }



    if (sceneId) {
        return getScene(sceneId);
    }
    else {
        return { getScene };
    }

};

export default
class Scene {

    constructor(id) {
        this.id = id;
    }

    getScene(id) {
        scene(id ? id : this.id);
    }
}
