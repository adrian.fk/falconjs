import View from "./View.js";

export default
class Page extends View {

    constructor(pageID, initialComponent, innerHtml) {
        super(pageID, innerHtml);

        this.components = [];
        this.initialComponent = initialComponent;
    }

    addComponent(component) {
        this.components.push(component);
    }

    removeAllComponents() {
        this.components.length = 0;
    }

    clear() {
        this.removeAllComponents();
        this.innerHtml = "";
    }

}