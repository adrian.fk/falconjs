

//A narrator for the root stage of the document

const Stage = (() => {

    const getElementById = (id) => {
        return document.getElementById(id);
    }

    const querySelector = (selector) => {
        return document.querySelector(selector);
    }

    const element = (identifier) => {
        let element = getElementById(identifier);
        if (element) return element;
        element = document.getElementsByClassName(identifier);
        if (element) return element
        element = document.getElementsByTagName(identifier);
        if (element) return element;
        element = querySelector(identifier);
        return element;
    }

    const setElementHtml = (elementId, html) => {
        if (element(elementId)) element(elementId).innerHTML = html;
    }


    return { setElementHtml, getElementById, querySelector, getElement: element };

}) ();

export default Stage;