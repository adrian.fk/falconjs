import Component from "./Component.js";


export default
class Image extends Component {

    constructor(id, classNames) {
        super(id, classNames);
    }

    setSrc(src) {
        this.setState({src: src});
    }

    setAlt(alt) {
        this.setState({alt: alt});
    }

    render() {
        if (!this.state.src) {
            return ""
        }
        else {
            return `
                <img src="${this.state.src}" alt="${this.state.alt}" />
            `
        }
    }
}