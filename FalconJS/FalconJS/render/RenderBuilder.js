

export default
class RenderBuilder {

    constructor(id, classNames) {
        this.id = id;
        this.classNames = classNames;
    }

    setClassNames(classNames) {
        this.classNames = classNames;
    }

    setId(id) {
        this.id = id;
    }



    makeDivWrapperContainer(content = "") {
        if ((this.id && this.classNames) && (this.id !== "" && this.classNames !== "")) {
            //Both id and classNames
            return `
                <div id="${this.id}" class="${this.classNames}">${content}</div>
            `
        }
        if ((!this.id || this.id === "") && (this.classNames && this.classNames !== "")) {
            //Only classNames
            return `
                <div class="${this.classNames}">${content}</div>
            `
        }
        if ((this.id && this.id !== "") && (!this.classNames || this.classNames === "")) {
            //Only id
            return `
                <div id="${this.id}">${content}</div>
            `
        }

        //Wrapper container without any specifications
        return `
                <div>${content}</div>
            `
    }
}