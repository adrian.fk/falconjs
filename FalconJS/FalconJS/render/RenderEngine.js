import RenderBuilder from "./RenderBuilder.js";


export default
class RenderEngine {
    static rootId = null;

    static setRootId(rootId = null) {
        RenderEngine.rootId = rootId;
    }

    static getRootContainer() {
        let rootContainer;
        if (RenderEngine.rootId) {
           rootContainer = document.getElementById(RenderEngine.rootId);
        }
        if (rootContainer) {
            return rootContainer;
        }
        rootContainer = document.body;

        return rootContainer;
    }

    static appendContentRoot(content) {
        const rootContainer = RenderEngine.getRootContainer();
        rootContainer.insertAdjacentHTML("beforeend", content);
    }

    static setContentRoot(content) {
        const rootContainer = RenderEngine.getRootContainer();
        rootContainer.innerHTML = content;
    }

    static renderView(view, stageID = "") {
        if (view) {
            let stage;
            if (stageID !== "") {
                stage = document.getElementById(stageID);
            }
            else {
                stage = document.getElementById(view.id);
            }
            if (view.beforeRender) view.beforeRender();
            this.renderToDomElement(stage, view.render());
            if (view.onRender) view.onRender();
        }
    }

    static renderToElementById(id, contentHtml) {
        const domElement = document.getElementById(id);
        if (domElement) domElement.innerHTML = contentHtml;
    }

    static renderToDomElement(domElement, contentHtml = "") {
        domElement.innerHTML = contentHtml;
    }

    static renderWrapperToRoot(id, contentHtml, boolSetRootContainer = false) {
        if (boolSetRootContainer) RenderEngine.setContentRoot(contentHtml);
        else RenderEngine.appendContentRoot(contentHtml);
    }

}