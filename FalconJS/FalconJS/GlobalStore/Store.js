

export default
class Store {

    constructor(storeName) {
        this.name = storeName;
        this.contents = new Map();
        this.contentsToIdMap = new Map();
    }

    get(id) {
        let content = this.contents.get(id);
        if (content) return content;
        else content = this.contents.get(this.contentsToIdMap.get(id));
        return content;
    }

    set(id, content) {
        const oldContent = this.contents.get(id);
        if (oldContent) {
            this.contentsToIdMap.delete(oldContent);
        }
        this.contents.set(id, content);
        this.contentsToIdMap.set(content, id);
        return this;
    }

    add(id, content) {
        return this.set(id, content);
    }

    remove(any) {

        let tarKey;
        let tar = this.contents.get(any);
        if (!tar) {
            tar = this.contents.get(this.contentsToIdMap.get(any));
            if (tar) {
                tarKey = this.contentsToIdMap.get(any);
            }
            else {
                return;
            }
        }
        else {
            tarKey = any;
        }
        this.contents.delete(tarKey);
        this.contentsToIdMap.delete(tar);
    }
}