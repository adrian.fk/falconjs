import Store from "./Store.js";
import Log from "../utills/FalconLogger.js";


const TAG = "GlobalStore";

const globalStore = new Store();

export function useGlobalStore(storeID) {
    const store = globalStore.get(storeID);
    if (store) return store;
    else {
        Log.d(TAG, "Could not find store with id: " + storeID);
    }
}

export function createGlobalStore(storeID) {
    const store = new Store(storeID);
    globalStore.add(storeID, store);
}