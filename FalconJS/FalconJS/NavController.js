import RenderEngine from "./render/RenderEngine.js";


const DEFAULT_CONTAINER_ID = "root";

const NavController = (() => {

    let container = DEFAULT_CONTAINER_ID;
    const edges = new Map();

    

    const addDestination = (id, dest) => {
        edges.set(id, dest);
    }

    const addDestinations = (destinations = [{id: "", dest: null}]) => {
        destinations.forEach(
            destination => {
                if (destination.id !== "" && destination.dest)
                    addDestination(destination.id, destination.dest);
            }
        );
    }

    const navTo = (destID) => {
        const tarDest = edges.get(destID);
        if (tarDest && edges.keys.length > 0) {
            if (container !== DEFAULT_CONTAINER_ID) RenderEngine.setRootId(container);
            let stage = RenderEngine.getRootContainer();
            if (tarDest.beforeRender) tarDest.beforeRender();
            RenderEngine.renderToDomElement(stage, tarDest.render())
            if (tarDest.onRender) tarDest.onRender();
        }
    }

    const setContainerID = (containerId) => {
        container = containerId;
    }

    return {
        addDestination,
        navTo,
        setContainerID,
    }

}) ();

export default NavController;
